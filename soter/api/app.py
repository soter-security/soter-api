"""
Module providing the ASGI app for soter-image-scan.
"""

from pkg_resources import iter_entry_points

from quart import Quart

from jsonrpc.server import Dispatcher
from jsonrpc.server.adapter.quart import websocket_blueprint

from . import cache


dispatcher = Dispatcher()
# Add the RPC modules to the dispatcher
from . import info
dispatcher.register_all(info, prefix = 'info')
from .image import rpc as image
dispatcher.register_all(image, prefix = 'image')
from .config import rpc as config
dispatcher.register_all(config, prefix = 'config')
from .namespace import rpc as namespace
dispatcher.register_all(namespace, prefix = 'namespace')


# Build the Quart app
app = Quart(__name__)
# Register the JSON-RPC blueprint
app.register_blueprint(websocket_blueprint(dispatcher), url_prefix = '/')

# Attach a cache instance to the app
@app.before_serving
async def create_cache():
    """
    Attach a cache object to the app.
    """
    app.cache = await cache.from_environment()
