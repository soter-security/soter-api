#####
## Dockerfile for the Soter API
#####

FROM python:3.7-alpine AS build

# Install package dependencies
#   * git for setuptools_scm
#   * build-base for packages that need to be built from source
RUN apk add --no-cache build-base git

# Build wheels for the dependencies
COPY requirements.txt /application/
RUN pip wheel \
      --wheel-dir /wheelhouse \
      --no-deps \
      --requirement /application/requirements.txt
# Then build a wheel for the app itself
COPY . /application
RUN pip wheel \
      --wheel-dir /wheelhouse \
      --no-deps \
      /application


FROM sotersec/soter-asgi

# Install the wheels built in the previous stage
COPY --from=build /wheelhouse /wheelhouse
RUN pip install --no-deps /wheelhouse/*.whl

# Ensure that the API runs as the ASGI user
USER $ASGI_UID
# Run the Soter API app
CMD ["/asgi-run.sh", "soter.api:app"]
